const mainUrl = "https://gitlab.com/nkming2/nc-photos/-/wikis/home";
const peopleUrl = "https://gitlab.com/nkming2/nc-photos/-/wikis/help/people";
const sharedAlbumLimitationsUrl =
    "https://gitlab.com/nkming2/nc-photos/-/wikis/help/shared-album#limitations";
const twoFactorAuthUrl =
    "https://gitlab.com/nkming2/nc-photos/-/wikis/help/two-factor-authentication";
const homeFolderNotFoundUrl =
    "https://gitlab.com/nkming2/nc-photos/-/wikis/help/home-folder-not-found";
